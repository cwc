.PHONY: all clean release

VERSION=1.0.2

all:
	cd src && $(MAKE) cwc
	ln -fs src/cwc ./cwc

clean:
	rm -fv cwc
	cd src && $(MAKE) clean

release: clean
	git clean -f -x
	( cd .. && tar chzvf cwc-$(VERSION).tar.gz --exclude .git cwc-$(VERSION) )
